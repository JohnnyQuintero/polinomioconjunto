/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author JHONY QUINTERO
 */
public class Termino_Algebraico {

    private char literal;
    private int valor;
    private int exponente;

    public Termino_Algebraico() {

    }

    public Termino_Algebraico(char literal, int valor, int exponente) {
        this.literal = literal;
        this.valor = valor;
        this.exponente = exponente;
    }

    public char getLiteral() {
        return literal;
    }

    public void setLiteral(char literal) {
        this.literal = literal;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getExponente() {
        return exponente;
    }

    public void setExponente(int exponente) {
        this.exponente = exponente;
    }

    @Override
    public String toString() {
        String msg = "";
        if (this.getValor() > 0) {
            msg += " + ";
        }
        msg += this.getValor() + "" + this.getLiteral() + "^" + this.getExponente();
        return msg;
    }

    public Termino_Algebraico getSumar(Termino_Algebraico t2) {
        if (this.getExponente() == t2.getExponente() && this.getLiteral() == t2.getLiteral()) {
            return new Termino_Algebraico(this.literal, this.valor + t2.valor, this.exponente);
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.literal;
        hash = 97 * hash + this.valor;
        hash = 97 * hash + this.exponente;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Termino_Algebraico other = (Termino_Algebraico) obj;
        if (this.literal != other.literal) {
            return false;
        }
        if (this.exponente != other.exponente) {
            return false;
        }
        return true;
    }

}
