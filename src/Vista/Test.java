/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Termino_Algebraico;
import Negocio.Polinomio;
import Negocio.Sistema_Polinomio;

/**
 *
 * @author JHONY QUINTERO
 */
public class Test {

    public static void main(String[] args) {
        try {
            Polinomio ecuacion = new Polinomio(4);
            Polinomio ecuacion1 = new Polinomio(4);
            Termino_Algebraico termino = new Termino_Algebraico('x', 4, 9);
            Termino_Algebraico termino1 = new Termino_Algebraico('x', 2, 7);
            Termino_Algebraico termino2 = new Termino_Algebraico('x', -5, 3);
            Termino_Algebraico termino3 = new Termino_Algebraico('x', 7, 0);
            ecuacion.addTermino(termino);
            ecuacion.addTermino(termino1);
            ecuacion.addTermino(termino2);
            ecuacion.addTermino(termino3);
            //*****************************************************************/
            Termino_Algebraico termino4 = new Termino_Algebraico('x', 4, 9);
            Termino_Algebraico termino5 = new Termino_Algebraico('x', 2, 7);
            Termino_Algebraico termino6 = new Termino_Algebraico('x', -4, 10);
            Termino_Algebraico termino7 = new Termino_Algebraico('x', -4, 0);
            ecuacion1.addTermino(termino4);
            ecuacion1.addTermino(termino5);
            ecuacion1.addTermino(termino6);
            ecuacion1.addTermino(termino7);
            //System.out.println(ecuacion.getSumarPolinomio(ecuacion1));
            /**
             * **********************************************
             */
            Sistema_Polinomio sistema = new Sistema_Polinomio(2);
            sistema.addPolinomio(ecuacion);
            sistema.addPolinomio(ecuacion1);
            System.out.println(ecuacion.toString());
            System.out.println("\t+");
            System.out.println(ecuacion1.toString());
            System.out.println("\t-----------------------------");
            System.out.println(sistema.getSumar());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }
}
