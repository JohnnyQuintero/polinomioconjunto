/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Termino_Algebraico;
import java.util.Arrays;

/**
 *
 * @author JHONY QUINTERO
 */
public class Polinomio {

    private Termino_Algebraico[] ecuacion;
    private int i;

    public Polinomio() {
        i = 0;
    }

    public Polinomio(int n) {
        this.ecuacion = new Termino_Algebraico[n];
    }

    public void addTermino(Termino_Algebraico nuevo) {
        if (this.getCapacidad() >= this.ecuacion.length) {
            throw new RuntimeException("Estructuta llena");
        }
        this.ecuacion[i] = nuevo;
        i++;
    }

    public String getDerivar() {
        if (this.getCapacidad() <= 0) {
            throw new RuntimeException("No hay ecuacion para derivar Bobo");
        }
        String msg = "Derivada -> ";
        for (int j = 0; j < this.getCapacidad(); j++) {
            float base, expo;
            base = (this.ecuacion[j].getValor() * this.ecuacion[j].getExponente());
            expo = (this.ecuacion[j].getExponente() - 1);
            if (expo != 0 && this.ecuacion[j].getExponente() != 0) {
                if (base >= 0) {
                    if (j != 0) {
                        msg += " + ";
                    }
                    msg += base + " " + this.ecuacion[j].getLiteral() + "^" + expo + " ";
                } else {
                    msg += base + " " + this.ecuacion[j].getLiteral() + "^" + expo + " ";
                }
            } else {
                if (base >= 0) {
                    msg += " + " + base + " ";
                } else {
                    msg += base + " ";
                }
            }
        }
        return msg;
    }

    public Polinomio getSumarPolinomio(Polinomio p2) {
        Polinomio nuevo = new Polinomio(this.getCapacidad()+p2.getCapacidad());
        for (int j = 0; j < this.getCapacidad(); j++) {
            if(this.ecuacion[j].equals(p2.ecuacion[j])){
                nuevo.addTermino(this.ecuacion[j].getSumar(p2.ecuacion[j]));
            }else{
                nuevo.addTermino(this.ecuacion[j]);
                nuevo.addTermino(p2.ecuacion[j]);
            }
        }
        return nuevo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Arrays.deepHashCode(this.ecuacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Polinomio other = (Polinomio) obj;
        for (int j = 0; j < this.getCapacidad(); j++) {
            for (int k = 0; k < other.getCapacidad(); k++) {
                if (!(this.ecuacion[j].equals(other.ecuacion[k]))) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String msg = "Polinomio -> ";
        for (int j = 0; j < this.getCapacidad(); j++) {
            msg += this.ecuacion[j].toString() + " ";
        }
        return msg;
    }

    public Termino_Algebraico[] getEcuacion() {
        return ecuacion;
    }

    public void setEcuacion(Termino_Algebraico[] ecuacion) {
        this.ecuacion = ecuacion;
    }

    public int getCapacidad() {
        return i;
    }
}
