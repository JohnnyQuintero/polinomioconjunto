/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Util.Conjunto;

/**
 *
 * @author JHONY QUINTERO
 */
public class Sistema_Polinomio {

    Conjunto<Polinomio> polinomio;

    public Sistema_Polinomio() {
    }

    public Sistema_Polinomio(int n) {
        this.polinomio = new Conjunto(n);
    }

    public void addPolinomio(Polinomio nuevo) throws Exception {
        this.polinomio.adicionarElemento(nuevo);
    }

    public Polinomio getSumar() throws Exception {
        for (int i = 0; i < this.polinomio.getCapacidad() - 1; i++) {
            return this.polinomio.get(i).getSumarPolinomio(polinomio.get(i + 1));
        }
        return null;
    }

    @Override
    public String toString() {
        return this.polinomio.toString();
    }

}
