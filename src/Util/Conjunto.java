/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author JHONY QUINTERO
 */
public class Conjunto<T> {
     //Estructura de datos estática
    private Caja<T>[] cajas;
    private int i = 0;

    public Conjunto() {
    }

    public Conjunto(int cantidadCajas) {
        if (cantidadCajas <= 0) {
            throw new RuntimeException("No se pueden crear el Conjunto");
        }
        this.cajas = new Caja[cantidadCajas];
    }

    public void adicionarElemento(T nuevo) throws Exception {
        if (i >= this.cajas.length) {
            throw new Exception("No hay espacio en el Conjunto");
        }
        this.cajas[i] = new Caja(nuevo);
        this.i++;

    }

    public T get(int indice) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }
        return this.cajas[indice].getObjeto();

    }

    public int indexOf(T objBuscar) {

        for (int j = 0; j < i; j++) {
            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(objBuscar)) {
                return j;
            }
        }
        return -1;
    }

    public void set(int indice, T nuevo) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }
        this.cajas[indice].setObjeto(nuevo);
    }

    public boolean existeElemento(T nuevo) {

        //Sólo estoy comparando por los estudiantes matriculados
        for (int j = 0; j < i; j++) {
            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(nuevo)) {
                return true;
            }
        }
        return false;
    }
        @Override
    public String toString() {
        String msg = "******** CONJUNTO*********\n";
        for (Caja c : cajas) {
            if (c != null) {
                msg += c.getObjeto().toString() + "\n";
            }
        }
        msg += "\n";
        return msg;
    }

    /**
     * Obtiene la cantidad de elementos almacenados
     *
     * @return retorna un entero con la cantidad de elementos
     */
    public int getCapacidad() {
        return this.i;
    }

    /**
     * Obtiene el tamaño máximo de cajas dentro del Conjunto
     *
     * @return int con la cantidad de cajas
     */
    public int getLength() {
        return this.cajas.length;
    }

}
